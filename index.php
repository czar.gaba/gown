<?php include('functions.php');?>
<?php include($partials.'header.php');?>
<?php 
// uncomment for session auto start
// session_starter();
?>
<body class="<?php fileclass();?>">
 
<div id="fullpage">
<div class="section section1" id="section1">

    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="jumbotron fadeInDown">
                <h1>Project Clothing </h1>
                <p>
                    <span>Lorem ipsum dolor sit amet consectetur adipisicing elit. Delectus harum molestiae, labore sed totam cum veniam enim blanditiis dolorem, esse alias necessitatibus placeat at nostrum. Aspernatur quo nesciunt eum aperiam!</span>
                    <span>Id quos molestiae saepe libero vel praesentium autem neque sint temporibus ullam expedita quibusdam omnis harum, quae vero magnam mollitia, nesciunt itaque quia! Ea ad beatae, atque iusto quaerat corporis!</span>
                    <span>Sed quisquam harum ut soluta quaerat blanditiis provident nisi et facilis voluptate quo pariatur velit saepe rerum explicabo doloribus voluptatem labore ea, illo eaque. Voluptas delectus officiis inventore quam! Ab.</span>
                </p>
                <a href="#section2" class="btn btn-primary">Continue..</a>
                </div>
            </div>
            <div class="col-md-6">
                <img src="assets/images/logo.jpg" alt="" class="img-fluid">
            </div>
        </div>
    </div>

</div>



<div class="section section2">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
            <div class="jumbotron">
                <h1>Shopping Section </h1>
                <p>Intruducing dresses designed to go together-and flatter every friend</p>
                <a href="shop.php" class="btn btn-warning btn-lg">Shop Now!</a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="section section3">

<div class="aboutus-area">
    <div class="container">
        <div class="row">
            <div class="">
            
            <div class="col-md-8 col-sm-6 col-xs-12" style="float:left;">
                <div class="aboutus-content ">
                    <h1>About <span>Us</span></h1>
                    <h4 style="color:#000;">Gown Project</h4>
                    <br> </br>
                    <h1>Mission</h1>
                    <p>PRimo fasion couture aim to equip individuals with skills they require to handle the creative process of fashion. Personalized the own concept or designs of the customers. WE want the whole world to dress in style and experience the unique designs. </p>
                    <br> </br>
                    <h1> Vision </h1>
                    <p>To dress up the whole world and to satisfy every customer with the quality and price of our design and clothing. </p>
                    
                </div>
            </div>
            <div class="col-md-4" style="float:left;">
               <!-- contact section  -->
  <!--Form with header-->

  
                    <!--Form with header-->
               <!-- end contact section  -->
            </div>    
            </div>
        </div>
    </div>
</div>

</div>

<!-- end fullpage -->
</div>

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">

 <?php include($partials.'footer.php');?>