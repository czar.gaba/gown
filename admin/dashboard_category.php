<?php include('partials/header.php');
$fileclass = fileclass2();

if($_SERVER['REQUEST_METHOD']=="POST"){
    if(isset($_POST['create'])){

        $array = array(
            'category' => $_POST['category'],
            'gender' => $_POST['gender']
            );


        if(insert($array,'tbl_category')){
        	?>
            <script>alert('created');</script>
            <?php
        }else{
        	?>
            <script>alert('error');</script>
            <?php
        }
        //end create
    }
    //end post
}

?>


<body class="dark-edition">
  <div class="wrapper ">
    <?php include('partials/sidenav.php');?>
    <div class="main-panel">
      <!-- Navbar -->
      <?php include('partials/mainnav.php');?>
      
	  <div class="content">

        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <h1>Category Management</h1>
                </div>
                <div class="col-md-12">
                <a href="?createcat=1" class="btn btn-primary btn-lg btn-create">Create Category</a>
                </div>
            </div>


            <div class="row">
                <?php if(isset($_GET['createcat'])){
                ?>
                    <div class="col-md-12">
                        
                        <form action="<?php echo $_SERVER['PHP_SELF'];?>" enctype="multipart/form-data" method = "post">
                        <input type="hidden" name="create" value="create" >
                        
                        <label for="title"></label><br>
                        
                        <label for="title">Category Name</label><br>
                        <input type="text" name="category" required><br>

                        <label for="title">Gender</label><br>
                        <select name="gender" id="">
                        <option value="male">male</option>
                        <option value="female">female</option>
                        <option value="uni">uni</option>
                        </select>
                        


               
                            <button type="submit" value="submit" class="btn btn-primary btn-lg">Create</button>
                            </form>
                        
                    </div>
                <?php
                }elseif(isset($_GET['updatecat'])){

                }else{
                    ?>
                    <div class="col-md-12">
                    <table class="table">
                        <thead>
                            <tr>
                            <th scope="col">cat id</th>
                            <th scope="col">Category</th>
                            <th scope="col">Gender</th>
                            <th scope="col">Options</th>
                            </tr>
                        </thead>
                        <tbody>
                            

                            <?php 
                           
                            $data = get('tbl_category');
                            foreach ($data as $row) {
                                // echo $row['name']."<br />\n";

                                ?>
                            <tr>
                            <th scope="row"><?php echo $row['id'];?></th>
                            <td><?php echo $row['category'];?></td>
                            <td><?php echo $row['gender'];?></td>
                            <td> <a href="delete.php?id=<?php echo $row['id'];?>&file=<?php echo $fileclass;?>&table=tbl_category" class="btn btn-danger btn-lg"> Delete </a></td>
                            </tr>
                                <?php
                            }
                            ?>
                        
                           
                            
                        </tbody>
                    </table>
                    </div>
                    <?php
                }
                ?>
            </div>


        </div>

      </div>
	  
    <?php include('partials/footer.php');?>
