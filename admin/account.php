<?php include('partials/header.php');?>


<body class="dark-edition">
  <div class="wrapper ">
    <?php include('partials/sidenav.php');?>
    <div class="main-panel">
      <!-- Navbar -->
      <?php include('partials/mainnav.php');?>
      
	  <div class="content">

        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <h1>Change password</h1>
                </div>
                <div class="col-md-12">
                    <form action="<?php echo $_SERVER['PHP_SELF'];?>" method="post">
                        <input type="password" name="password" required>
                        <button type="submit" value="submit" class="btn btn-primary btn-lg">update<div class="ripple-container"></div></button>
                    </form>
                </div>
            </div>
        </div>

      </div>
	  

      <?php 
      
      if($_SERVER['REQUEST_METHOD']=="POST"){
        // update function usage
        $array = array(
            'password'=> $_POST['password']
        );
        if(update($array,$id,'tbl_user')){
           ?>
                <script>alert('updated');</script>
           <?php
        }else{
            echo 'error on update';
        }
        
      }
      
      ?>


    <?php include('partials/footer.php');?>
