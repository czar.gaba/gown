<div class="sidebar" data-color="purple" data-background-color="black" >
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    -->
      <div class="logo"><a href="./" class="simple-text logo-normal">
          Dashboard
        </a></div>
      <div class="sidebar-wrapper">
        <ul class="nav">

        <?php 
        if($access == 'admin'){
?>

          <li class="nav-item active  ">
            <a class="nav-link" href="./">
              <i class="material-icons">dashboard</i>
              <p>Dashboard</p>
            </a>
          </li>

          <li class="nav-item ">
            <a class="nav-link" href="dashboard_items.php">
              <p>Item Management</p>
            </a>
          </li>

          <li class="nav-item ">
            <a class="nav-link" href="dashboard_category.php">
              <p>Category</p>
            </a>
          </li>

          <li class="nav-item ">
            <a class="nav-link" href="dashboard_transactions.php">
              <p>Transactions</p>
            </a>
          </li>


          
<?php
        }elseif($access == 'guest'){
          ?>
          <li class="nav-item ">
            <a class="nav-link" href="dashboard_transactions.php">
              <p>View My Orders</p>
            </a>
          </li>
          <?php
        }else{}
        ?>
          


          <li class="nav-item ">
            <a class="nav-link" href="../shop.php">
              <p>Shop Page</p>
            </a>
          </li>
          <li class="nav-item ">
            <a class="nav-link" href="account.php">
              <p>Change Password</p>
            </a>
          </li>
          <li class="nav-item ">
            <a class="nav-link" href="logout.php">
              <p>Logout</p>
            </a>
          </li>
         

        </ul>
      </div>
    </div>