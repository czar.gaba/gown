<?php include('partials/header.php');
$fileclass = fileclass2();
?>

<?php 
if($_SERVER['REQUEST_METHOD']=="POST"){
    if(isset($_POST['create'])){


        $fileName = $_FILES['image']['name'];
        $tmpName = $_FILES['image']['tmp_name'];
        $fileSize = $_FILES['image']['size'];
        $fileType = $_FILES['image']['type'];
        $filePath =  $upload_dir . $fileName;
        $filePath2 =  $upload_dir2 . $fileName;
        $result = move_uploaded_file($tmpName, $filePath);
        if($result){



            $data = get_where_fieldvalue('tbl_category','category', $_POST['category']);
            foreach ($data as $row) {
                $gender = $row['gender'];
            }

            $array = array(
                'name' => $_POST['name'],
                'price' => $_POST['price'],
                'description' => $_POST['desc'],
                'category' => $_POST['category'],
                'qty' => $_POST['qty'],
                'gender' => $gender,
                'image' => $filePath2
                );

          if(insert($array,'tbl_items')){
            ?>
            <script>alert('Success');</script>
            <?php
            header('refresh 2s;url=dashboard_items.php');
          }else{
            header('location:error.php');
          }

        }
        //end create
    }
    if(isset($_POST['update'])){

        $fileName = $_FILES['image']['name'];
    $tmpName = $_FILES['image']['tmp_name'];
    $fileSize = $_FILES['image']['size'];
    $fileType = $_FILES['image']['type'];
    $filePath =  $upload_dir . $fileName;
    $filePath2 =  $upload_dir2 . $fileName;
    $result = move_uploaded_file($tmpName, $filePath);
    if($result){
        $data = get_where_fieldvalue('tbl_category','category', $_POST['category']);
            foreach ($data as $row) {
                $gender = $row['gender'];
            }
        $array = array(
            'name' => $_POST['name'],
            'price' => $_POST['price'],
            'description' => $_POST['desc'],
            'category' => $_POST['category'],
            'qty' => $_POST['qty'],
            'gender' => $gender,
            'image' => $filePath2
            );

      if(update($array,$_POST['updateid'],'tbl_items')){
        ?>
        <script>alert('Success Update');</script>
        <?php
        header('refresh 2s;url=items.php');
      }else{
        header('location:error.php');
      }

    }
    }
}
?>


<body class="dark-edition">
  <div class="wrapper ">
    <?php include('partials/sidenav.php');?>
    <div class="main-panel">
      <!-- Navbar -->
      <?php include('partials/mainnav.php');?>
      
	  <div class="content">

        <div class="container-fluid">

            <div class="row">
                <div class="col-md-12">
                    <h1>ITEM MANAGMENT</h1>
                </div>
                <div class="col-md-12">
                    <a href="?additem=1" class="btn btn-lg btn-info btn-create">Add Item</a>
                </div>
            </div>

            <div class="row">
            
                <?php 
                    if(isset($_GET['additem'])){
                      ?>
<div class="col-md-12">
                        <h2 style="color:#fff;"> Add Item Form</h2>
                        <style> .btn-create{display:none;}</style>

                        <form action="<?php echo $_SERVER['PHP_SELF'];?>" enctype="multipart/form-data" method = "post">
                        <input type="hidden" name="create" value="create" >
                                
                        <label for="title">Item Name</label><br>
                        <input type="text" name="name" required><br>

                        <label for="title">Price</label><br>
                        <input type="number" name="price" required><br>

                        <label for="title">QTY</label><br>
                        <input type="number" name="qty" required><br>

                        <label for="title">Category</label><br>
                        <select name="category" id="">
                        <?php 
                        
                        $data = get('tbl_category');
                        foreach ($data as $row) {
                            // echo $row['name']."<br />\n";
                            ?>
                            <option value="<?php echo $row['category'];?>"> <?php echo $row['category'];?> </option>
                            <?php
                        }
                        ?>
                        
                        </select>
                        
                        <label for="content">description</label>
                        <textarea name="desc" id="" cols="30" rows="10" class="form-control textarea " ></textarea>

                        
                        <input id="avatar-1" name="image" type="file" class="file-loading" accept="image/*" required>
                        <br><br>
                        <button type="submit" value="submit" class="btn btn-primary btn-lg">Create</button>
                        </form>
                        </div>

                      <?php 
                    }elseif(isset($_GET['updateitem'])){
                    //   update 

                    
          //start update
          $updateid = $_GET['updateitem'];

          // GET where id 
          $data = get_whereid('tbl_items' , $updateid);
          foreach ($data as $row) {
             if(isset($row)){
                // echo "update set";
                ?>
<div class="col-md-12">
<h2 style="color:#fff;"> Update Item Form</h2>
                        <style> .btn-create{display:none;}</style>
<form action="<?php echo $_SERVER['PHP_SELF'];?>" enctype="multipart/form-data" method = "post">
          <input type="hidden" name="update" value="update" >
          <input type="hidden" name="updateid" value="<?php echo $row['id'];?>" >
          
          
          <label for="title">Event Title</label><br>
          <input type="text" name="name" value="<?php echo $row['name'];?>" required><br>

          <label for="title">Price</label><br>
          <input type="number" name="price" value="<?php echo $row['price'];?>" required><br>
          
          <label for="title">QTY</label><br>
          <input type="number" name="qty" value="<?php echo $row['qty'];?>" required><br>


          <label for="title">Category</label><br>
            <select name="category" id="">
            <?php 
            
            $datax = get('tbl_category');
            foreach ($datax as $rowx) {
                // echo $row['name']."<br />\n";
                ?>
                <option value="<?php echo $rowx['category'];?>"> <?php echo $rowx['category'];?> </option>
                <?php
            }
            ?>
            
            </select>

          <label for="content"></label>
          <textarea name="desc" id="" cols="30" rows="10" class="form-control textarea " ><?php echo $row['description'];?></textarea>

            
            <input id="avatar-1" name="image" type="file" accept="image/*" class="file-loading" required>
            <br><br>
            <button type="submit" value="submit" class="btn btn-primary btn-lg">Update</button>
            </form>
            </div>
                <?php

             }else{
                header('location:error.php');
             }
          }
          //end update
                    }else{
                    //display 

                    
                    $data = get('tbl_items');
                    foreach ($data as $row) {
                    ?>

<div class="card mb-3" style="max-width: 540px;">
  <div class="row no-gutters">
    <div class="col-md-4">
      <img src="../<?php echo $row['image'];?>" class="card-img" alt="...">
    </div>
    <div class="col-md-8">
      <div class="card-body">
        <h3 class="card-title"><strong><?php echo $row['name'];?></strong></h3>
        <p class="card-text">
        <?php 
        $text = display_excerpt($row['description'] , 100);
        echo $text;
        ?></p>
        <a href="delete.php?id=<?php echo $row['id'];?>&file=<?php echo $fileclass;?>&table=tbl_items" class="btn btn-danger btn-lg"> Delete </a>
        <a href="?updateitem=<?php echo $row['id'];?>" class="btn btn-warning btn-lg"> Update </a>
      </div>
    </div>
  </div>
</div>
                    <?php
                    }

                    //end else
                    }
                ?>
            
            </div>

        </div>

      </div>
	  
    <?php include('partials/footer.php');?>
