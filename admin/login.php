<?php include('functions.php');?>
<?php 
session_starter();
if(isset($_SESSION['user'])){
	header('location: ./');
}
?>
<?php 

if($_SERVER['REQUEST_METHOD']=="POST"){

  $conn = getConnection();
  $u = strip_tags($_POST['user']);
	$p = strip_tags($_POST['pass']);

	if (isset($u,$p)){

	try {
		$str= "select * from `tbl_user` where username=:u and password=:p";
		$cm=$conn->prepare($str);
		$cm->bindParam(':u', $u);
		$cm->bindParam(':p', $p);
		$cm->execute();
		$user = $cm->rowcount();
		
		if ($user == 0) {
			?>
      <script>alert('login failed');</script>
      <?php 
			header("refresh:1;url=./");
			
		}else{
      ?>
      <script>alert('login success');</script>
      <?php 
      $_SESSION['user'] = $u;
      echo $_SESSION['user'] ;

      #die();
      header("refresh:1;url=./");
    die();
  }


	} catch (Exception $e) {
		echo 'error  '.$e ->getmessage();
	}
	
	}



  
  

  //end post
}

?>


<link rel="stylesheet" href="../node_modules/bootstrap/dist/css/bootstrap.min.css">
<link rel="stylesheet" href="assets/css/user.css">

<div class="container">
  <div class="row">
    <div class="col-md-3"></div>
    <div class="col-md-6">
      <!-- form  -->
      <div class="wrapper fadeInDown">
  <div id="formContent">
    <!-- Tabs Titles -->
    <br>
    <!-- Icon -->
    <div class="fadeIn first">

      <img src="assets/img/user.png" id="icon" alt="User Icon" /><br>
      <h3>Login Form</h3>
    </div>
    <br>
    <!-- Login Form -->
    <form action="<?php echo $_SERVER['PHP_SELF'];?>" method="POST">
    <fieldset>
	    <?php
			if (isset($_SESSION['error_msg'])) {
		?>
		<div class="alert alert-warning" style="margin: 24px;">
		<?= $_SESSION['error_msg'] ?>
		</div>
		<?php
		}
		unset($_SESSION['error_msg']);
		?>
	</fieldset>

      <input type="text" id="login" class="fadeIn second" name="user" placeholder="Username">
      <input type="password" id="password" class="fadeIn third" name="pass" placeholder="Password">
      
      <input type="submit" class="fadeIn fourth" value="Log In">
    </form>

    <!-- Remind Passowrd -->
    <div id="formFooter">
      <a class="underlineHover" href="../">Home</a> | 
      <a class="underlineHover" href="register.php">Register</a>
    </div>

  </div>
      <!-- end form  -->
    </div>
    <div class="col-md-3"></div>
  </div>
</div>