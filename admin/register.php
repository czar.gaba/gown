<?php include('functions.php');?>
<?php 
session_starter();
if(isset($_SESSION['user'])){
	header('location: ./');
}
?>
<?php 

if($_SERVER['REQUEST_METHOD']=="POST"){

  $conn = getConnection();
  $u = strip_tags($_POST['user']);
	$p = strip_tags($_POST['pass']);

	if (isset($u,$p)){

	try {
		$str= "select * from `tbl_user` where username=:u and password=:p";
		$cm=$conn->prepare($str);
		$cm->bindParam(':u', $u);
		$cm->bindParam(':p', $p);
		$cm->execute();
		$user = $cm->rowcount();
		
		if ($user == 0) {
	
			  // insert function usage
              $array = array(
                'username'=>$_POST['user'],
                'password'=>$_POST['pass'],
                'access'=>$_POST['access'],
                'email'=>$_POST['email'],
                'address'=>$_POST['address'],
                'contact'=>$_POST['contact'],
                'gender'=>$_POST['gender']
            );
            if(insert($array,'tbl_user')){
                echo "<script>alert('account created')</script>";
                echo "<script>window.open('login.php','_self')</script>";
            }else{
                echo 'error on insert';
            }
			
		}else{
      ?>
      <script>alert('account already created');</script>
      <script>window.open('register.php','_self')</script>
      <?php 
    
  }


	} catch (Exception $e) {
		echo 'error  '.$e ->getmessage();
	}
	
	}



  
  

  //end post
}

?>


<link rel="stylesheet" href="../node_modules/bootstrap/dist/css/bootstrap.min.css">
<link rel="stylesheet" href="assets/css/user.css">

<div class="container">
  <div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-10">
      <!-- form  -->
      <div class="wrapper fadeInDown">
  <div id="formContent">
    <!-- Tabs Titles -->
    <br>
    <!-- Icon -->
    <div class="fadeIn first">

      <img src="assets/img/user.png" id="icon" alt="User Icon" /><br>
      <h3>Register Form</h3>
    </div>
    <br>
    <!-- Login Form -->
    <form action="<?php echo $_SERVER['PHP_SELF'];?>" method="POST">
    <fieldset>
	    <?php
			if (isset($_SESSION['error_msg'])) {
		?>
		<div class="alert alert-warning" style="margin: 24px;">
		<?= $_SESSION['error_msg'] ?>
		</div>
		<?php
		}
		unset($_SESSION['error_msg']);
		?>
	</fieldset>

      <input type="text" id="login" class="fadeIn second" name="user" placeholder="Username" required>
      <input type="password" id="password" class="fadeIn third" name="pass" placeholder="Password">

      <input style="    background-color: #f6f6f6;
    border: none;
    color: #0d0d0d;
    padding: 15px 32px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 5px;
    width: 85%;
    border: 2px solid #f6f6f6;
    -webkit-transition: all 0.5s ease-in-out;
    -moz-transition: all 0.5s ease-in-out;
    -ms-transition: all 0.5s ease-in-out;
    -o-transition: all 0.5s ease-in-out;
    transition: all 0.5s ease-in-out;
    -webkit-border-radius: 5px 5px 5px 5px;
    border-radius: 5px 5px 5px 5px;" type="email" id="login" class="fadeIn second" name="email" placeholder="email" required>
      <input type="text" id="login" class="fadeIn second" name="contact" placeholder="contact" required>
      <textarea style="    background-color: #f6f6f6;
    border: none;
    color: #0d0d0d;
    padding: 15px 32px;
    text-align: left;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 5px;
    width: 85%;
    border: 2px solid #f6f6f6;
    -webkit-transition: all 0.5s ease-in-out;
    -moz-transition: all 0.5s ease-in-out;
    -ms-transition: all 0.5s ease-in-out;
    -o-transition: all 0.5s ease-in-out;
    transition: all 0.5s ease-in-out;
    -webkit-border-radius: 5px 5px 5px 5px;
    border-radius: 5px 5px 5px 5px;" name="address" id="" cols="30" rows="10">Address</textarea>
    <select style="    background-color: #f6f6f6;
    border: none;
    color: #0d0d0d;
    padding: 15px 32px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 5px;
    width: 85%;
    border: 2px solid #f6f6f6;
    -webkit-transition: all 0.5s ease-in-out;
    -moz-transition: all 0.5s ease-in-out;
    -ms-transition: all 0.5s ease-in-out;
    -o-transition: all 0.5s ease-in-out;
    transition: all 0.5s ease-in-out;
    -webkit-border-radius: 5px 5px 5px 5px;
    border-radius: 5px 5px 5px 5px;" name="gender" id="">
    <option value="male">male</option>
    <option value="female">female</option>
    </select>
      <input type="hidden" value="guest" name="access">
      <input type="submit" class="fadeIn fourth" value="Log In">
    </form>

    <!-- Remind Passowrd -->
    <div id="formFooter">
      <a class="underlineHover" href="login.php">Login</a>
    </div>

  </div>
      <!-- end form  -->
    </div>
    <div class="col-md-3"></div>
  </div>
</div>