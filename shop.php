<?php include('functions.php');?>
<?php include($partials.'header.php');?>
<?php 
// uncomment for session auto start
 session_starter();
 cart();
?>
<body class="<?php fileclass();?>">


<?php include($partials.'navbar.php');?>

<div class="container">

<div class="row">

  <div class="col-lg-4">

   
  
    <?php 
    
    include($partials.'category.php');
    ?>


  </div>
  <!-- /.col-lg-3 -->

  <div class="col-lg-8" style="">

  <?php 
     if(isset($_SESSION["shopping_cart"])){
      include($partials.'cart.php');
     }else{
      include($partials.'carousel.php');
     }
    
    ?>

    <div class="row">


  <?php 
  

  if(!isset($_GET['category'])){
    $data = get('tbl_items');
  }elseif($_GET['category']==""){
    $data = get('tbl_items');
  }else{
    $data = get_where_fieldvalue('tbl_items','category', $_GET['category']);
  }

foreach ($data as $row) {
    // echo $row['name']."<br />\n";
    ?>
 <div class="col-lg-6 col-md-6 mb-4">
        <div class="card h-100">
        <a href="product.php?id=<?php echo $row['id'];?>"><img class="card-img-top" src="<?php echo $row['image'];?>" alt=""></a>
          <div class="card-body">
            <h4 class="card-title">
              <a href="product.php?id=<?php echo $row['id'];?>"><?php echo $row['name']; ?></a>
            </h4>
            <h5>P <?php echo $row['price'];?> | QTY : <?php echo $row['qty'];?></h5>
            <p class="card-text"><?php 
            $text = display_excerpt($row['description'] , 200);
            echo $text;?></p>
          </div>
          <div class="card-footer">
          <a href="product.php?id=<?php echo $row['id'];?>">view product</a>
          </div>
        </div>
      </div>
    <?php
}

  ?>



     
      

    </div>
    <!-- /.row -->

  </div>
  <!-- /.col-lg-9 -->

</div>
<!-- /.row -->

</div>


<?php include($partials.'footer.php');?>