-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 16, 2019 at 04:55 PM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.1.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gown`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_category`
--

CREATE TABLE `tbl_category` (
  `id` int(11) NOT NULL,
  `category` text NOT NULL,
  `gender` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_category`
--

INSERT INTO `tbl_category` (`id`, `category`, `gender`) VALUES
(2, 'barong', 'male'),
(3, 'Maria Clara Costume', 'female'),
(4, 'Saya', 'female'),
(10, 'Evening Gown', 'female'),
(11, 'Wedding Gown', 'female'),
(13, 'Suit Coat', 'male');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_items`
--

CREATE TABLE `tbl_items` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `price` text NOT NULL,
  `description` text NOT NULL,
  `category` text NOT NULL,
  `gender` text NOT NULL,
  `image` text NOT NULL,
  `qty` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_items`
--

INSERT INTO `tbl_items` (`id`, `name`, `price`, `description`, `category`, `gender`, `image`, `qty`) VALUES
(10, 'Ethnic Manobo Attire', '1500', 'Manobo simply means \"people\" or person alternate names include\" Manuvu\" and \"Minivu\". The term may have originated from \"Mansuba\", a combination of man and suba river. Manobos are concentrated in Agusan, Bukidnon, Cotabato, Davao. Misamis Oriental, and Surigao Del Sur of the Philippines.', 'Saya', 'female', 'assets/images/ethnic manobo attire.jpg', '15'),
(11, 'Yello Dress', '2500', 'Yellow defines HAPPINES..Slay and share happiness with this catriona x Maxine inspired yellow dress in the runway..', 'Maria Clara Costume', 'female', 'assets/images/yellow dress.jpg', '10'),
(12, 'Man\'s Suite', '3500', 'Formal wear is the traditional western dress code category applicable for the most formal occasions.', 'trousers', 'male', 'assets/images/man\'s suite.jpg', '20'),
(13, 'White evening gown', '2500', 'This elegant white evening gown symbolizes purity and innocence. It is also a sign of femininity and innocence.', 'Evening Gown', 'female', 'assets/images/white evening gown.jpg', '15'),
(14, 'Wedding Gown', '15000', ' Wedding dress or bridal gown is the clothing worn by a bride during a wedding ceremony. Color, style and ceremonial importance of the gown can depend on the religion and culture of the wedding participants.', 'Wedding Gown', 'female', 'assets/images/Wedding Gown.jpg', '10'),
(16, 'Barong Black', '4999', 'A subtle ethnic pattern and the black color only enhance the superior nature of this high-quality jusi fabric barong tagalog shirt.', 'barong', 'male', 'assets/images/barong black.jpg', '6'),
(17, 'AQUA BLUE JUSILYN BARONG TAGALOG', '2749', 'A sharp style for an impeccable formal look', 'barong', 'male', 'assets/images/aqua blue jusilyn barong tagalog.jpg', '5'),
(18, 'BLACK-SILVER ORGANZA BARONG TAGALOG', '3250', 'New design of embroidery complete with monochromatic color.', 'barong', 'male', 'assets/images/BLACK-SILVER ORGANZA BARONG TAGALOG.jpg', '7'),
(19, 'BROWN BARONG TAGALOG', '4999', 'Modern look with our hand-painted design and with the matching chinese collar and chinese buttoned down barong tagalog. ', 'barong', 'male', 'assets/images/BROWN BARONG TAGALOG.jfif', '6'),
(20, 'BURGUNDY JUSILYN BARONG TAGALOG', '2749', 'Traditional straight point collar, cuff buttons', 'barong', 'male', 'assets/images/BURGUNDY JUSILYN BARONG TAGALOG.jpg', '6'),
(21, 'COAT BARONG TAGALOG', '8000', 'Five-button closure, three buttoned cuffs and full lining adds structure and weight to drape smoothly.', 'barong', 'male', 'assets/images/COAT BARONG TAGALOG.jpg', '5'),
(22, 'SKOPES CLASSIC SUIT JACKET GREY', '9011', 'A Superbly tailored, comfortable & hard wearing contemporary suit for modern day living', 'Suit Coat', 'male', 'assets/images/SKOPES CLASSIC SUIT JACKET GREY.jpg', '6'),
(23, 'MENS BLAZER SUIT', '1107', 'Lightweight suit jacket coats business lapel suit for business wedding', 'Suit Coat', 'male', 'assets/images/Mens Blazer suite.jpg', '5'),
(24, 'Casual sport coat slim fit blazer jacket', '2499', 'Design in slim fit featuring with notched lapel, one button closure left chest real pocket with white fake handkerchief.', 'Suit Coat', 'male', 'assets/images/Casual sport coat slim fit blazer jacket.jpg', '6'),
(25, 'WEEN CHARM', '2499', 'Mens casual slim fit standing collar blazer 3 button suit sport jackets', 'Suit Coat', 'male', 'assets/images/WEEN CHARM.jpg', '6'),
(26, 'VOBAGA SUIT', '1399', 'Men slim fit stylish casual one button suit coat jacket business blazers.', 'Suit Coat', 'male', 'assets/images/Vobaga.jpg', '6'),
(27, 'COOFANDY ', '2599', 'Slim fit suit blazer jacket two button lightweight sports coat.', 'Suit Coat', 'male', 'assets/images/COOFANDY suit.jpg', '6'),
(28, 'Maria clara fully beaded gown', '2200', 'Cream/Transparent', 'Maria Clara Costume', 'female', 'assets/images/maria clara fully beaded gown.jfif', '6'),
(29, 'Mestiza Maria clara gown', '5600', 'The boat neck is accentuated with an alluring beaded design and its features.', 'Maria Clara Costume', 'female', 'assets/images/mestiza gown.jpg', '6'),
(30, 'VEINELLERY', '1299', 'The fuchsia national filipiniana outfit.', 'Maria Clara Costume', 'female', 'assets/images/97694c54b146c553bc4d72eb45d9f6c6.jfif', '6'),
(31, 'POSTURA MARIA CLARA GOWN', '3800', 'Pineapple fiber top with embroidery and sheer black long skirt.', 'Maria Clara Costume', 'female', 'assets/images/evening gown.jpg', '6'),
(32, 'Baro\'t Saya', '3200', 'Filipino national costume is rather colorful, ornate and beautiful. Filipino people lived under the occupation of other countries for centuries. That\'s why their national attire was formed.', 'Saya', 'female', 'assets/images/baro\'t saya.jpg', '5'),
(33, 'FILIPINIANA', '2590', 'The term itself comes from the Tagalong words \"barot at saya\" or \"blouse and skirt,\" still the basic components of the ensemble.', 'Saya', 'female', 'assets/images/FILIPINIANA.jpg', '6'),
(34, 'FILIPINIA DRESS', '3700', 'Filipiniana dress traje de mestiza or simply terno is a traditional dress worn by women in the Philippines. .', 'Saya', 'female', 'assets/images/dress.jpg', '6'),
(35, 'FILIPINIANA BARO\'T SAYA', '2800', 'The top or baro is normally made of piña fiber and accompanied with a separate skirt known as the saya. Unlike the terno, the baro\'t saya has more components.', 'Maria Clara Costume', 'female', 'assets/images/das.jpg', '6'),
(36, 'RED PROM DRESS', '5500', 'Red prom dresses with high slits and deep v-necklines to charming short red prom dresses with beading, ', 'Evening Gown', 'female', 'assets/images/Red.jpg', '5'),
(37, 'RAW SILK TULLE SKIRT TOP IN LIGHT BLUE COLOUR', '3000', 'The Stylish And Elegant Skirt Top In Navy Blue Colour Looks Stunning And Gorgeous With Trendy And Fashionable Sequins Work Stone Work Beads', 'Evening Gown', 'female', 'assets/images/BB.jpg', '5'),
(38, 'ALLURING NAVY BLUE NET WEDDING WEAR', '5600', 'The Stylish And Elegant Skirt Top In Navy Blue Colour Looks Stunning And Gorgeous With Trendy And Fashionable Sequins Work,Stone Work,Beads', 'Evening Gown', 'female', 'assets/images/VAVA.jpg', '5'),
(39, 'BLACK EVENING DRESS', '8000', 'An evening gown, evening dress or gown is a long flowing dress usually worn to formal occasions. The drop ranges from ballerina (mid-calf to just above the ankles), tea (above the ankles), to full-length.', 'Evening Gown', 'female', 'assets/images/BLACK EVENING DRESS.jpg', '4'),
(40, 'RED EVENING GOWN', '2500', 'The Red Bombshell Evening Gown Dress Striking yet understated. Could this be the perfect red evening dress? Crafted using the finest ..', 'Evening Gown', 'female', 'assets/images/ASASADSA.jpg', '5'),
(41, 'Wedding gown with sleeves', '10000', 'Ball gown wedding dresses classic,vintage wedding dresses princess,ball gown wedding dresses illusion', 'Wedding Gown', 'female', 'assets/images/fafa.jpg', '3'),
(42, 'WHITE VICTORIAN STYLE WEDDING GOWN', '10000', 'A fabulous vintage 70s soft white wedding gown that takes you back to the victorian era! A fabulous vintage 70s soft white wedding gown that takes you back to the victorian era!', 'Wedding Gown', 'female', 'assets/images/gaga.jpg', '4'),
(43, 'WEDDING DRESS', '12000', 'Bridal gown is the clothing worn by a bride during a wedding ceremony. Color, style and ceremonial importance of the gown can depend on the religion and culture of the wedding participants.', 'Wedding Gown', 'female', 'assets/images/SDSAFQGA.jpg', '4');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_transaction`
--

CREATE TABLE `tbl_transaction` (
  `id` int(11) NOT NULL,
  `itemname` text NOT NULL,
  `quantity` text NOT NULL,
  `size` text NOT NULL,
  `price` text NOT NULL,
  `location` text NOT NULL,
  `costumer` text NOT NULL,
  `date` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_transaction`
--

INSERT INTO `tbl_transaction` (`id`, `itemname`, `quantity`, `size`, `price`, `location`, `costumer`, `date`) VALUES
(16, 'test barong<br>maria clara huhubarin', '89<br>48', 'small<br>small', '10,100.00', 'tuguegarao', 'danny', '2019-12-16 04:57:55');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL,
  `username` text NOT NULL,
  `password` text NOT NULL,
  `access` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`id`, `username`, `password`, `access`) VALUES
(1, 'admin', 'admin', 'admin'),
(2, 'test', 'test', 'guest'),
(3, 'danny', '12345', 'guest');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_category`
--
ALTER TABLE `tbl_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_items`
--
ALTER TABLE `tbl_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_transaction`
--
ALTER TABLE `tbl_transaction`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_category`
--
ALTER TABLE `tbl_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `tbl_items`
--
ALTER TABLE `tbl_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `tbl_transaction`
--
ALTER TABLE `tbl_transaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
