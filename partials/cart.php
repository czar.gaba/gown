<?php if(isset($_SESSION["shopping_cart"])){
?>
<div class="cart">
<h3>Order Details</h3>
    <table class="table ">
        <tr>
            <th >Item Name</th>
            <th >Item ID</th>
            <th >Quantity</th>
            <th >Price</th>
            <th >Total</th>
            <th >size</th>
            <th >Action</th>
        </tr>
        <?php
        if(!empty($_SESSION["shopping_cart"]))
        {
            $total = 0;
            foreach($_SESSION["shopping_cart"] as $keys => $values)
            {
        ?>
        <tr>
            <td><?php echo $values["item_name"]; ?></td>
            <td><?php echo $values["item_id"]; ?></td>
            <td><?php echo $values["item_quantity2"]; ?></td>
            <td>P <?php echo $values["item_price"]; ?></td>
            <td>P <?php echo number_format($values["item_quantity2"] * $values["item_price"], 2);?></td>
            <td><?php echo $values["item_size"]; ?></td>
            <td><a href="shop.php?action=delete&id=<?php echo $values["item_id"]; ?>"><span class="text-danger">Remove</span></a></td>
        </tr>
        <?php
                $total = $total + ($values["item_quantity2"] * $values["item_price"]);
            }
        ?>
        <tr>
            <td colspan="4" align="right">Total</td>
            <td align="right">P <?php echo number_format($total, 2); ?></td>
            <td><a href="payment.php" class="btn btn-primary">pay now</a></td>
        </tr>
        <?php
        }
        ?>
            
    </table>

    
</div>
<?php
}?>