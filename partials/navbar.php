<?php session_starter();?>
<div class="hm-gradient">
<nav class="mb-4 navbar navbar-expand-lg navbar-dark unique-color-dark">
                <a class="navbar-brand" href="./">Project Clothing</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-4" aria-controls="navbarSupportedContent-4" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent-4">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link waves-effect waves-light" href="./">  Home</a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link waves-effect waves-light" href="shop.php"><i class="fa fa-heart"></i> Shopping Page <span class="sr-only">(current)</span></a>
                        </li>

                        <li class="nav-item active">
                            <a class="nav-link waves-effect waves-light" href="contact.php"><i class="fa fa-heart"></i> Contact <span class="sr-only">(current)</span></a>
                        </li>

                        <li class="nav-item active">
                            <a class="nav-link waves-effect waves-light" href="about.php"><i class="fa fa-heart"></i> About <span class="sr-only">(current)</span></a>
                        </li>


                        <li class="nav-item">
                        <?php 
                            if(isset($_SESSION['user'])){
                                ?>
                                <a class="nav-link waves-effect waves-light" href="./admin"> <?php echo $_SESSION['user']; ?></a>
                                <?php
                            }else{
                                ?>
                                <a class="nav-link waves-effect waves-light" href="./admin">  Log In</a>
                                <?php
                            }
                        ?>

                            <!-- <a class="nav-link waves-effect waves-light" href="./admin">  Log In</a> -->
                        </li>
                        <!-- <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle waves-effect waves-light" id="navbarDropdownMenuLink-4" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-newspaper-o"></i> Media </a>
                            <div class="dropdown-menu dropdown-menu-right dropdown-cyan" aria-labelledby="navbarDropdownMenuLink-4">
                                <a class="dropdown-item waves-effect waves-light" href="#">Facebook</a>
                                <a class="dropdown-item waves-effect waves-light" href="#">Instagram</a>
                            </div>
                        </li> -->
                    </ul>
                </div>
            </nav>
            </div>