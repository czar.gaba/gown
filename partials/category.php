<h1 class="my-4">Shop Now!</h1>

<div class="showall">
<h2>General</h2>
<div class="list-group">
<a href="?category=" class="list-group-item">showall</a>
</div>
</div>

<h2>Male</h2>
<div class="list-group">
<?php 
$data = get_where_fieldvalue('tbl_category','gender','male');
foreach ($data as $row) {
  ?>
<a href="?category=<?php echo $row['category']; ?>" class="list-group-item"><?php echo $row['category']; ?></a>
  <?php
}
?>
</div>

<h2>Female</h2>
<div class="list-group">
<?php 
$data = get_where_fieldvalue('tbl_category','gender','female');
foreach ($data as $row) {
  ?>
<a href="?category=<?php echo $row['category']; ?>" class="list-group-item"><?php echo $row['category']; ?></a>
  <?php
}
?>
</div>


<h2>Uni</h2>
<div class="list-group">
<?php 
$data = get_where_fieldvalue('tbl_category','gender','uni');
foreach ($data as $row) {
  ?>
<a href="?category=<?php echo $row['category']; ?>" class="list-group-item"><?php echo $row['category']; ?></a>
  <?php
}
?>
</div>