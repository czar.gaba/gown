<?php

//Declarations

$dir = dirname(__FILE__);
$partials = $dir."/partials/";

// auto import php files on includes folder
foreach (glob("includes/*.php") as $filename){
    include $filename;
}

function cart(){

    if(isset($_POST["add_to_cart"]))
    {
        if(isset($_SESSION["shopping_cart"]))
        {
            $item_array_id = array_column($_SESSION["shopping_cart"], "item_id");
            if(!in_array($_GET["id"], $item_array_id))
            {
                $count = count($_SESSION["shopping_cart"]);
                $item_array = array(
                    'item_id'			=>	$_GET["id"],
                    'item_name'			=>	$_POST["hidden_name"],
                    'item_price'		=>	$_POST["hidden_price"],
                    'item_quantity'		=>	$_POST["qty1"],
                'item_quantity2'		=>	$_POST["qty2"],
                    'item_size'		=>	$_POST["size"]
                );
                $_SESSION["shopping_cart"][$count] = $item_array;
            }
            else
            {
                foreach($_SESSION["shopping_cart"] as &$value){
                    if($value['item_id'] === $_GET["id"]){
                        $value['item_quantity2'] = $_POST["qty2"] + $value['item_quantity2'] ;
                        break; // Stop the loop after we've found the product
                    }
                }
            }
        }
        else
        {
            $item_array = array(
                'item_id'			=>	$_GET["id"],
                'item_name'			=>	$_POST["hidden_name"],
                'item_price'		=>	$_POST["hidden_price"],
                'item_quantity'		=>	$_POST["qty1"],
                'item_quantity2'		=>	$_POST["qty2"],
                'item_size'		=>	$_POST["size"]
            );
            $_SESSION["shopping_cart"][0] = $item_array;
        }
    }
    
    if(isset($_GET["action"]))
    {
        if($_GET["action"] == "delete")
        {
            foreach($_SESSION["shopping_cart"] as $keys => $values)
            {
                if($values["item_id"] == $_GET["id"])
                {
                    unset($_SESSION["shopping_cart"][$keys]);
                    echo '<script>alert("Item Removed")</script>';
                    echo '<script>window.location="shop.php"</script>';
                }
                if(empty($_SESSION["shopping_cart"])){
                    unset($_SESSION["shopping_cart"]);
                }
						
                
            }
        }
    }
  
    
}