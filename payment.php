<?php include('functions.php');?>
<?php include($partials.'header.php');?>
<?php 
// uncomment for session auto start
 session_starter();
?>


<body class="<?php fileclass();?>">
<?php include($partials.'navbar.php');?>

<div class="container">
<div class="cart">
            <h3>Order Details</h3>
				<table class="table table-bordered">
					<tr>
						<th width="40%">Item Name</th>
						<th width="10%">Quantity</th>
						<th width="20%">Price</th>
						<th width="20%">size</th>
						<th width="15%">Total</th>
						<th width="5%">Action</th>
					</tr>
					<?php
					if(!empty($_SESSION["shopping_cart"]))
					{
						$total = 0;
						foreach($_SESSION["shopping_cart"] as $keys => $values)
						{
							$list_items[] = $values["item_name"];
							$list_size[] = $values["item_size"];
							$list_qty[] = $values["item_quantity2"];
					?>
					<tr>
						<td><?php echo $values["item_name"]; ?></td>
						<td><?php echo $values["item_quantity2"]; ?></td>
						<td>P <?php echo $values["item_price"]; ?></td>
						<td><?php echo $values["item_size"]; ?></td>
						<td>P <?php echo number_format($values["item_quantity2"] * $values["item_price"], 2);?> </td>
						<td><a href="shop.php?action=delete&id=<?php echo $values["item_id"]; ?>"><span class="text-danger">Remove</span></a></td>
					</tr>
					<?php
							$total = $total + ($values["item_quantity2"] * $values["item_price"]);
							$xxx = $total+100;
						}
					?>
					<tr>
						<td colspan="4" align="right">Note Delivery Fee = P 100.00 |Total</td>
                        <td align="right">P <?php echo number_format($xxx, 2); 
                        $_SESSION['TOTAL'] = number_format($xxx, 2);
                        ?></td>
						<td></td>
					</tr>
					<?php
					}
					?>
						
				</table>
			</div>
</div>





<div class="container">

    <?php 
// print_r($values["item_name"]);
    ?>
    
    <div class="columns twelve">
	<?php 
$items = implode('<br>',$list_items);
// echo $items."<br>";
$size = implode('<br>',$list_size);
$qty = implode('<br>',$list_qty);
// echo $size;
		?>
    <form action="<?php echo $_SERVER['PHP_SELF'];?>" method="post">
        <input type="hidden" name="item" value="<?php echo $items;?>">
		<input type="hidden" name="size" value="<?php echo $size;?>">
		<input type="hidden" name="qty" value="<?php echo $qty;?>">
        <input type="hidden" name="total-price" value="<?php echo $_SESSION['TOTAL'];?>">
        <input type="hidden" name="user" value="<?php echo $_SESSION['user'];?>">
		
	
		<?php
			if(isset($_SESSION['user'])){
?>
		<h2>Costumer: <?php echo $_SESSION['user'];?></h2>
		<!-- <input type="text" name="costumer" class="form-control"> -->
		<input type="hidden" name="costumer" value="<?php echo $_SESSION['user'];?>">
<?php
			}else{
?>
		<h2>Costumer Name</h2>
		<input type="text" name="costumer" class="form-control">
<?php
			}

			// get where field
$data = get_where_fieldvalue('tbl_user','username',$_SESSION['user']);
foreach ($data as $row) {
	// echo $row['name']."<br />\n";
	$address = $row['address'];
	$contact = $row['contact'];
	$gender = $row['gender'];
}

		?>
		<h2>gender: <?php echo $gender;?></h2>
        <h2>contact: <?php echo $contact;?></h2>
        <h2>Billing Address</h2>
        <textarea name="address" id="" style="width:100%;" class="form-control" required><?php echo $address;?></textarea>
        <input type="submit" value="pay now!" class="btn btn-primary" style="margin-top:30px;">
    </form>

    </div>



</div>



<?php if($_SERVER['REQUEST_METHOD']=="POST"){








	$conn = getConnection();
    if(isset($_POST['total-price'])){
 $st = "INSERT INTO `tbl_transaction`(`itemname`, `quantity`, `size`, `price`, `location`, `costumer`, `date`,`status`) VALUES (:i,:q,:s,:p,:a,:c,:d,:s)";
    $cm=$conn->prepare($st);
    // $cm->bind_param(
    
    // $_POST['user'],
    // $_POST['total-price'],
    // 'CASH ON DELIVERY',
    // $_POST['item'],
    // date('Y-m-d H:i:s'));

    // $cm->bindvalue(':u', $_POST['user']);
    $cm->bindvalue(':p', $_POST['total-price']);
    $cm->bindvalue(':c', $_POST['costumer']);
	$cm->bindvalue(':i', $_POST['item']);
	$cm->bindvalue(':s', $_POST['size']);
	$cm->bindvalue(':q', $_POST['qty']);
    $cm->bindvalue(':d', date('Y-m-d H:i:s'));
    $cm->bindvalue(':a', $_POST['address']);
	$cm->bindvalue(':s', 'pending');
    if($cm->execute()){
		foreach($_SESSION["shopping_cart"] as $keys => $values){

			$idx = $values["item_id"];
			$qty1 = $values["item_quantity"];
			$qty2 = $values["item_quantity2"];
			$qty3 = $qty1 - $qty2;
			$array = array(
				'qty'=> $qty3
			);
			if(update($array,$idx,'tbl_items')){
			
			}else{
				echo 'error on update';
			}
			
		
		}
		?>
		<script>alert('checkout');</script>
		<?php
		unset($_SESSION["shopping_cart"]);
		 echo "<script>window.open('shop.php','_self')</script>";
	}else{
		?>
		<script>alert('failed');</script>
		<?php
    }
}else{echo '<script>alert("empty cart")</script>';}
   
}?>
 <?php include($partials.'footer.php');?>